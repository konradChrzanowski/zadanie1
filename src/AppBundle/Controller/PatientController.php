<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Clinic;
use AppBundle\Entity\Patient;
use AppBundle\Form\PatientType;
use AppBundle\Form\PeselType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PatientController extends Controller
{

    /**
     * @Route("/editPatient/{id}", name="editPatient")
     */
    public function editAction($id, Request $request){

        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository('AppBundle:Patient')->find($id);
        $clinic = $patient->getClinics();

        if (!$patient) {
            throw $this->createNotFoundException('Patient not found with id = '.$id);
        }

        $formPatient = $this->createForm(new PatientType(), $patient);
        $formPatient->handleRequest($request);

        if ($formPatient->isValid()){
            $em->flush();
        }


        $delete = $this->createFormBuilder($patient)
            ->add('delete', 'submit', array('label' => 'Delete this patient'))
            ->getForm();
        $delete->handleRequest($request);

        if($delete->isValid()){
            $em->remove($patient);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }

        return $this->render('patient/edit.html.twig', array(
            'id' => $id,
            'formPatient' => $formPatient->createView(),
            'delete' => $delete->createView(),
            'patient' => $patient,
            'clinic' => $clinic,
        ));
    }

}