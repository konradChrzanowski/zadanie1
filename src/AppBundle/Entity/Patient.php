<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Annotations\DocLexer;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as PatientAssert;

/** 
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Patient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $firstName;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $lastName;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @PatientAssert\Pesel
     */
    private $pesel;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $dateBirth;


    /**
     * @ORM\ManyToMany(targetEntity="Clinic", cascade={"persist"})
     */
    protected $clinics;

    public function __construct()
    {
        $this->clinics = new ArrayCollection();
    }
    
    /** 
     * @ORM\PrePersist 
     * @ORM\PreUpdate
     */
    public function Capitalize()
    {
        $this->firstName = ucfirst($this->firstName);
        $this->lastName = ucfirst($this->lastName); 
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Patient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Patient
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set pesel
     *
     * @param string $pesel
     * @return Patient
     */
    public function setPesel($pesel)
    {
        $this->pesel = $pesel;

        return $this;
    }

    /**
     * Get pesel
     *
     * @return string 
     */
    public function getPesel()
    {
        return $this->pesel;
    }

    /**
     * Set dateBirth
     *
     * @param \DateTime $dateBirth
     * @return Patient
     */
    public function setDateBirth($dateBirth)
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }

    /**
     * Get dateBirth
     *
     * @return \DateTime 
     */
    public function getDateBirth()
    {
        return $this->dateBirth;
    }

    /**
     * Add clinics
     *
     * @param \AppBundle\Entity\Clinic $clinics
     * @return Patient
     */
    public function addClinic(\AppBundle\Entity\Clinic $clinics)
    {
        $this->clinics[] = $clinics;

        return $this;
    }

    /**
     * Remove clinics
     *
     * @param \AppBundle\Entity\Clinic $clinics
     */
    public function removeClinic(\AppBundle\Entity\Clinic $clinics)
    {
        $this->clinics->removeElement($clinics);
    }

    /**
     * Get clinics
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClinics()
    {
        return $this->clinics;
    }
}
