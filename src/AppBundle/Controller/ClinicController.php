<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Clinic;
use AppBundle\Entity\Patient;
use AppBundle\Form\PatientType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ClinicController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getRepository('AppBundle:Clinic');

        $clinics = $em
            ->findAll();

        return $this->render('clinic/index.html.twig', array(
            'clinics' => $clinics,
        ));
    }


    
    /**
     * @Route("/new", name="new")
     */
    public function newAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        $clinic = new Clinic();
        $form = $this->createFormBuilder($clinic)
            ->add('name', 'text')
            ->add('save', 'submit', array('label' => 'Add'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $clinic = $form->getData();

            $em->persist($clinic);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }


        return $this->render('clinic/add.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $clinic = $em
            ->getRepository('AppBundle:Clinic')
            ->find($id);

        if (!$id) {
            throw $this->createNotFoundException('Clinic not found with id = '.$id);
        }

        //Adding patients
        $patient = new Patient();
        $formPatient = $this->createForm(new PatientType(), $patient);
        $formPatient->handleRequest($request);

        if ($formPatient->isValid()){
            $patient->AddClinic($clinic);            
            $em->persist($patient);
            $em->flush();
        }


        //Deleting clinic
        $delete = $this->createFormBuilder($clinic)
            ->add('delete', 'submit', array('label' => 'Delete this clinic'))
            ->getForm();
        $delete->handleRequest($request);

        if($delete->isValid()){
            $em->remove($clinic);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }


        return $this->render('clinic/edit.html.twig', array(
            'id' => $id,
            'formPatient' => $formPatient->createView(),
            'delete'      => $delete->createView(),
            'clinic' => $clinic,
        ));
    }

}