<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Pesel extends Constraint
{


    public $message = 'This value is not a valid PESEL number';


    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}


